# ========================= SETTINGS ============================
# Set mod as windows key
set $mod Mod4
set $term --no-startup-id $TERMINAL

set $up    "k"
set $down  "j"
set $right "l"
set $left  "h"

# Get colors from ~/.Xresources
set_from_resource $term_background background
set_from_resource $term_foreground foreground
set_from_resource $term_color0     color0
set_from_resource $term_color1     color1
set_from_resource $term_color2     color2
set_from_resource $term_color3     color3
set_from_resource $term_color4     color4
set_from_resource $term_color5     color5
set_from_resource $term_color6     color6
set_from_resource $term_color7     color7
set_from_resource $term_color8     color8
set_from_resource $term_color9     color9
set_from_resource $term_color10    color10
set_from_resource $term_color11    color11
set_from_resource $term_color12    color12
set_from_resource $term_color13    color13
set_from_resource $term_color14    color14
set_from_resource $term_color15    color15

for_window [class="^.*"] border pixel 3
gaps inner 8

# Smart gaps (gaps used if only more than one container on the workspace)
smart_gaps on

# Smart borders (draw borders around container only if it is not the only container on this workspace) 
# on|no_gaps (on=always activate and no_gaps=only activate if the gap size to the edge of the screen is 0)
smart_borders on

# Window colors
client.focused           $term_color14 $term_color14 $term_background $term_color14
client.unfocused         $term_color0  $term_color0  $term_foreground $term_color0
client.focused_inactive  $term_color0  $term_color0  $term_foreground $term_color0
client.urgent            $term_color1  $term_color1  $term_background $term_color1

# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

for_window [title="scratch"] floating enable
for_window [title="scratch"] resize set 700 525

# Define names for default workspaces for which we configure key bindings later on.
# We use variables to avoid repeating the names in multiple places.
set $ws1  "1" 
set $ws2  "2" 
set $ws3  "3" 
set $ws4  "4" 
set $ws5  "5" 
set $ws6  "6" 
set $ws7  "7" 
set $ws8  "8" 
set $ws9  "9" 
set $ws10 "10"

# Assign classes to specific workspaces
assign [class="^TelegramDesktop"] $ws9
# ===============================================================


# ========================= STARTUP =============================
exec --no-startup-id dropbox
exec --no-startup-id /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1
exec --no-startup-id nitrogen --restore; sleep 1; compton -b
exec --no-startup-id nm-applet
exec --no-startup-id xfce4-power-manager
exec --no-startup-id xautolock -time 20 -locker ~/.config/i3/lock.sh
exec_always --no-startup-id ff-theme-util
exec_always --no-startup-id fix_xcursor
exec --no-startup-id volumeicon
# ===============================================================


# ========================= KEYBINDINGS =========================
# start a terminal
bindsym $mod+Return exec $term

# Add bindings for switching monitors
bindsym $mod+Shift+b exec --no-startup-id monitor-built-in.sh 
bindsym $mod+Shift+v exec --no-startup-id monitor-dp.sh

# kill focused window
bindsym $mod+Shift+q kill

# Program launcher
# bindsym $mod+d exec --no-startup-id dmenu_run -i -p "run:"
bindsym $mod+d exec --no-startup-id "rofi -show combi -combi-modi 'window,run' -modi combi"

# screenkey
bindsym $mod+Delete exec --no-startup-id "killall screenkey || screenkey -s small"

# split in horizontal orientation
bindsym $mod+c split h

# split in vertical orientation
bindsym $mod+v split v

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

# toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# Stop screen recording
bindsym $mod+Shift+s exec --no-startup-id kill-screen-record.sh

# Take screenshot
bindsym $mod+Shift+t exec --no-startup-id screenshot.sh

# Start ranger
bindsym $mod+r exec $term -e ranger

# change container layout (stacked, tabbed, toggle split)
bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

# Switch to workspace
bindsym $mod+1 workspace $ws1
bindsym $mod+2 workspace $ws2
bindsym $mod+3 workspace $ws3
bindsym $mod+4 workspace $ws4
bindsym $mod+5 workspace $ws5
bindsym $mod+6 workspace $ws6
bindsym $mod+7 workspace $ws7
bindsym $mod+8 workspace $ws8
bindsym $mod+9 workspace $ws9
bindsym $mod+0 workspace $ws10

# Move focused container to workspace
bindsym $mod+Ctrl+1 move container to workspace $ws1
bindsym $mod+Ctrl+2 move container to workspace $ws2
bindsym $mod+Ctrl+3 move container to workspace $ws3
bindsym $mod+Ctrl+4 move container to workspace $ws4
bindsym $mod+Ctrl+5 move container to workspace $ws5
bindsym $mod+Ctrl+6 move container to workspace $ws6
bindsym $mod+Ctrl+7 move container to workspace $ws7
bindsym $mod+Ctrl+8 move container to workspace $ws8
bindsym $mod+Ctrl+9 move container to workspace $ws9
bindsym $mod+Ctrl+0 move container to workspace $ws10

# Move to workspace with focused container
bindsym $mod+Shift+1 move container to workspace $ws1; workspace $ws1
bindsym $mod+Shift+2 move container to workspace $ws2; workspace $ws2
bindsym $mod+Shift+3 move container to workspace $ws3; workspace $ws3
bindsym $mod+Shift+4 move container to workspace $ws4; workspace $ws4
bindsym $mod+Shift+5 move container to workspace $ws5; workspace $ws5
bindsym $mod+Shift+6 move container to workspace $ws6; workspace $ws6
bindsym $mod+Shift+7 move container to workspace $ws7; workspace $ws7
bindsym $mod+Shift+8 move container to workspace $ws8; workspace $ws8
bindsym $mod+Shift+9 move container to workspace $ws9; workspace $ws9
bindsym $mod+Shift+0 move container to workspace $ws10; workspace $ws10

# Move focus
bindsym $mod+$left  focus left
bindsym $mod+$down  focus down
bindsym $mod+$up    focus up
bindsym $mod+$right focus right

bindsym $mod+Shift+$left  move left 80
bindsym $mod+Shift+$down  move down 80
bindsym $mod+Shift+$up    move up 80
bindsym $mod+Shift+$right move right 80

bindsym $mod+Shift+y resize shrink width  70px or 5 ppt
bindsym $mod+Shift+u resize shrink height 70px or 5 ppt
bindsym $mod+Shift+i resize grow   height 70px or 5 ppt
bindsym $mod+Shift+o resize grow   width  70px or 5 ppt

# i3Lock script
bindsym $mod+x exec --no-startup-id ~/.config/i3/lock.sh

# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart

# Audio controls
bindsym XF86AudioRaiseVolume exec --no-startup-id pactl set-sink-volume 0 +3%
bindsym XF86AudioLowerVolume exec --no-startup-id pactl set-sink-volume 0 -3%
bindsym XF86AudioMute exec --no-startup-id pactl set-sink-mute 0 toggle

# Using F9, F10, F11 for Prev, Play/pause, Next
bindsym XF86Tools exec --no-startup-id playerctl previous
bindsym XF86Search exec --no-startup-id playerctl play-pause
bindsym XF86LaunchA exec --no-startup-id playerctl next

# Sreen brightness controls
bindsym XF86MonBrightnessUp exec --no-startup-id xbacklight -inc 10 # increase screen brightness
bindsym XF86MonBrightnessDown exec --no-startup-id xbacklight -dec 10 # decrease screen brightness

bindsym $mod+Ctrl+m exec pavucontrol
# ===============================================================

# Sample i3bar
bar {
	i3bar_command i3bar
	status_command i3status
	position top

  ## please set your primary output first. Example: 'xrandr --output eDP1 --primary'
  #	tray_output primary
  #	tray_output eDP1

	bindsym button4 nop
	bindsym button5 nop
  font xft:Meslo LG M 10
	strip_workspace_numbers yes

    colors {
        background $term_background
        statusline $term_foreground
        separator  $term_color0

                          # border            backgr.           text
        focused_workspace   $term_color8      $term_color8      $term_foreground
        active_workspace    $term_background  $term_background  $term_foreground
        inactive_workspace  $term_background  $term_background  $term_color8
        urgent_workspace    $term_color1      $term_color1      $term_foreground
    }
}
