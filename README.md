# dotfiles

![dirty-screenshot](https://i.imgur.com/IfbK8uV.png)
![neofetch-screenshot](https://i.imgur.com/BWZBLN2.png)

Some of the stuff I'm using: 
- [X11](https://github.com/seesleestak/dotfiles/blob/master/X11) 
- [i3](https://github.com/seesleestak/dotfiles/blob/master/i3/.config/i3/config) 
- [urxvt](https://github.com/seesleestak/dotfiles/blob/master/X11/.Xresources)
- [tmux](https://github.com/seesleestak/dotfiles/blob/master/tmux/.tmux.conf) 
- [vim](https://github.com/seesleestak/dotfiles/tree/master/vim/.vim) 
- [rofi](https://github.com/seesleestak/dotfiles/blob/master/rofi/.config/rofi/config)
- [ranger](https://github.com/seesleestak/dotfiles/blob/master/ranger/.config/ranger/rc.conf) 
